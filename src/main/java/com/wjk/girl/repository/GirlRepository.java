package com.wjk.girl.repository;

import com.wjk.girl.domain.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GirlRepository extends JpaRepository<Girl,Integer>{
    /**
    *@Autor 王帮主
    *@Description 通过年龄查询信息列表
    *@param
    *@Date: 21:28 2018/1/28
    */
    public List<Girl> findByAge(Integer age);
}
