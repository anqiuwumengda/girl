package com.wjk.girl.utils.wechat;

import com.thoughtworks.xstream.XStream;
import com.wjk.girl.domain.wechat.News;
import com.wjk.girl.domain.wechat.NewsMessage;
import com.wjk.girl.domain.wechat.TextMessage;
import javafx.scene.chart.PieChartBuilder;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MessageUtil {
    public static final String MESSAGE_TEXT = "text";
    public static final String MESSAGE_IMAGE = "image";
    public static final String MESSAGE_VOICE = "voice";
    public static final String MESSAGE_VIDEO = "video";
    public static final String MESSAGE_LINK= "link";
    public static final String MESSAGE_LOCATION = "location";
    public static final String MESSAGE_NEWS = "news";//图文消息
    public static final String MESSAGE_EVENT = "event";
    public static final String MESSAGE_SUBSCRIBE = "subscribe";
    public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe";
    public static final String MESSAGE_CLICK = "CLICK";
    public static final String MESSAGE_VIEW = "VIEW";
    /**
    *@Autor 王帮主
    *@Description 普通微信用户向公众账号发消息时，微信服务器将POST消息的XML数据包到开发者填写的URL上
     * 需要解析xml
    *@param 
    *@Date: 16:20 2018/5/31
    */
    public static Map<String,String> xmlToMap(HttpServletRequest request) throws IOException, DocumentException {
        Map<String,String> map = new HashMap<>();
        SAXReader reader = new SAXReader();

        InputStream ins = request.getInputStream();
        Document doc = reader.read(ins);
        Element root = doc.getRootElement();
        List<Element> list = root.elements();
        for(Element e : list){
            map.put(e.getName(),e.getText());
        }
        ins.close();
        return map;
    }
    /**
    *@Autor 王帮主
    *@Description 将对象类型转换为xml
    *@param
    *@Date: 16:40 2018/5/31
    */
    public static String textMessageToXml(TextMessage textMessage){
        XStream xStream = new XStream();//包含将对象类型转换为xml的方法
        xStream.alias("xml",textMessage.getClass());//xml根节点替换，不替换的话<com.wjk.girl.domain.wechat.TextMessage>
        return xStream.toXML(textMessage);
    }
    /**
    *@Autor 王帮主
    *@Description 主菜单
    *@param
    *@Date: 9:01 2018/6/1
    */
    public static String menuText(){
        StringBuffer sb = new StringBuffer();
        sb.append("欢迎您的关注，请按照菜单提示操作：\n\n");
        sb.append("1,公众号介绍\n");
        sb.append("2,图文消息\n\n");
        sb.append("回复？调出此菜单.");
        return sb.toString();
    }
    /**
    *@Autor 王帮主
    *@Description 拼接返回的文本消息
    *@param
    *@Date: 9:03 2018/6/1
    */
    public static String initText(String toUserName,String fromUserName,String content){
        TextMessage message = new TextMessage();
        message.setFromUserName(toUserName);
        message.setToUserName(fromUserName);
        message.setCreateTime(new Date().getTime()+"");
        message.setContent(content);
        message.setMsgType(MessageUtil.MESSAGE_TEXT);
        return textMessageToXml(message);
    }
    
    /**
    *@Autor 王帮主
    *@Description 图文消息组装
    *@param 
    *@Date: 11:55 2018/6/1
    */
    public static String initNewsMessage(String toUserName,String fromUserName){
        String message = null;
        NewsMessage newsMessage = new NewsMessage();
        List<News> newsList = new ArrayList<>();
        News news = new News();
        news.setTitle("");
        news.setDescription("王建坤，壮士王，人称安丘吴孟达。");
        news.setPicUrl("http://wangjiankun.tunnel.echomod.cn/girl/img/highlander.jpg");
        news.setUrl("https://gitee.com");
        newsList.add(news);
        newsMessage.setFromUserName(toUserName);
        newsMessage.setToUserName(fromUserName);
        newsMessage.setCreateTime(new Date().getTime()+"");
        newsMessage.setMsgType(MESSAGE_NEWS);
        newsMessage.setArticles(newsList);
        newsMessage.setArticleCount(newsList.size());
        message = newsMessageToXml(newsMessage);
        return message;

    }
    public static String firstMenu(){
        StringBuffer sb = new StringBuffer();
        sb.append("王建坤订阅号展示微信公众号功能，将持续更新。");
        return sb.toString();
    }
    public static String secondMenu(){
        StringBuffer sb = new StringBuffer();
        sb.append("王建坤，壮士王，人称安丘吴孟达。清朝康熙年间政治家、军事家，收复台湾的决定性人物之一。姚启圣为政带兵执法严明，" +
                "曾随康亲王爱新觉罗·杰书平定耿精忠叛乱，" +
                "在收复台湾战役中功勋卓着，历任福建总督、兵部尚书、太子太保等职。");
        return sb.toString();
    }

    /**
    *@Autor 王帮主
    *@Description 图文消息实体转换为xml
    *@param 
    *@Date: 11:20 2018/6/1
    */
    public static String newsMessageToXml(NewsMessage newsMessage){
        XStream xStream = new XStream();
        xStream.alias("xml",newsMessage.getClass());
        xStream.alias("item",new News().getClass());
        return xStream.toXML(newsMessage);
    }


}
