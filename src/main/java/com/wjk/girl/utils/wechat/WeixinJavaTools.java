package com.wjk.girl.utils.wechat;

import com.wjk.girl.properties.wechat.WechatAccountConfig;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.result.WxMediaUploadResult;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.message.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class WeixinJavaTools {
    public static void doService(WxMpXmlMessage message, HttpServletResponse response){
        WxMpService wxMpService = new WxMpServiceImpl();
        WxMpInMemoryConfigStorage wxConfigProvider = new WxMpInMemoryConfigStorage();
        wxConfigProvider.setAppId(new WechatAccountConfig().getMpAppId());
        wxConfigProvider.setSecret(new WechatAccountConfig().getMpAppSecret());
        wxMpService.setWxMpConfigStorage(wxConfigProvider);
        //消息处理
        String messageType=message.getMsgType();   //消息类型
        String fromUser=message.getFromUser();
        String touser=message.getToUser();
        String content=message.getContent();
        //文本消息

        if(content.equals("文本")){
            System.out.println("文本消息========");
            //创建消息文本
            WxMpXmlOutTextMessage text=WxMpXmlOutTextMessage.TEXT().toUser(fromUser).fromUser(touser).content("我是文本消息").build();

            String xml=text.toXml();
            System.out.println("xml:"+xml);

            PrintWriter out=null;
            try {
                out = response.getWriter();
                out.print(xml);
            } catch (IOException e) {
                out.close();
                out=null;
                e.printStackTrace();
            }
            out.close();
            out=null;
        }
        //图片消息
        else if(content.equals("图文")){

            WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
            item.setDescription("weixin-java-tools");
            item.setPicUrl("http://wangjiankun.tunnel.echomod.cn/girl/img/highlander.jpg");
            item.setTitle("壮士王");
            item.setUrl("www.baidu.com");

            WxMpXmlOutNewsMessage m = WxMpXmlOutMessage.NEWS()
                    .fromUser(touser)
                    .toUser(fromUser)
                    .addArticle(item)
                    .build();
            String xml = m.toXml();
            System.out.println("xml:"+xml);
            PrintWriter out = null;
            try {
                out = response.getWriter();
                out.print(xml);
            } catch (IOException e) {
                e.printStackTrace();
            }

            out.close();
            out = null;
        }else if(content.equals("图片")) {
            System.out.println("图片消息==============");

            //创建file对象
            File file = new File("D:\\imgs\\1.jpg");

            WxMediaUploadResult result = null;
            try {
                result = wxMpService.getMaterialService().mediaUpload(WxConsts.MediaFileType.IMAGE, file);
            } catch (WxErrorException e1) {
                e1.printStackTrace();
            }

            if (result != null) {
                String mediaId = result.getMediaId();

                WxMpXmlOutImageMessage image = WxMpXmlOutImageMessage.IMAGE().toUser(fromUser).fromUser(touser).mediaId(mediaId).build();

                String xml = image.toXml();
                System.out.println("xml:" + xml);

                PrintWriter out = null;
                try {
                    out = response.getWriter();
                    out.print(xml);
                } catch (IOException e) {
                    out.close();
                    out = null;
                    e.printStackTrace();
                }
                out.close();
                out = null;
            }
        }
    }
}
