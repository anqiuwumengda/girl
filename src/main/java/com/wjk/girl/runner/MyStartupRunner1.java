package com.wjk.girl.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
*@Autor 王帮主
*@Description  CommandLineRunner  接口是在容器启动成功后的最后一步回调（类似开机自启动）
*@param 
*@Date: 18:07 2018/4/27
*/
@Component
@Order(value = 2)
public class MyStartupRunner1 implements CommandLineRunner{
    @Override
    public void run(String... strings) throws Exception {
        System.out.println("MyStartupRunner1(CommandLineRunner)启动:"+strings);
    }
}
