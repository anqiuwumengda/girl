package com.wjk.girl.handle;

import com.wjk.girl.domain.Result;
import com.wjk.girl.exception.GirlException;
import com.wjk.girl.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
*@Autor 王帮主
*@Description  异常统一处理
*@param
*@Date: 15:04 2018/1/29
*/
@ControllerAdvice//异常统一处理注解
public class ExceptionHandle {
    private Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    /**
    *@Autor 王帮主
    *@Description 捕获异常的方法，捕获完成后，返回给浏览器Result对象
    *@param
    *@Date: 15:06 2018/1/29
    */
    @ExceptionHandler(value = Exception.class)//捕获的异常类型
    @ResponseBody
    public Result handle(Exception e){
        if(e instanceof GirlException){
            GirlException girlException = (GirlException) e;
            return ResultUtil.error(girlException.getCode(),girlException.getMessage());
        }else {
            logger.info("【系统异常】{}",e);
            return ResultUtil.error(-1,"未知错误");
        }
    }
}
