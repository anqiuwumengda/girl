package com.wjk.girl.domain.wechat;

import com.fasterxml.jackson.databind.ser.Serializers;

public class TextMessage extends BaseMessage{

    private String Content;
    private String MsgId;
    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String msgId) {
        MsgId = msgId;
    }
}
