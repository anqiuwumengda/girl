package com.wjk.girl.domain.wechat;

public class ImageMessage extends BaseMessage{
    private Image Image;

    public com.wjk.girl.domain.wechat.Image getImage() {
        return Image;
    }

    public void setImage(com.wjk.girl.domain.wechat.Image image) {
        Image = image;
    }
}
