package com.wjk.girl.domain;

import javax.swing.*;

/**
*@Autor 王帮主
*@Description http请求返回的最外层对象
*@param
*@Date: 13:57 2018/1/29
*/
public class Result<T> {
    /** 错误代码*/
    private Integer code;
    /**提示信息*/
    private String msg;
    /**具体内容*/
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
