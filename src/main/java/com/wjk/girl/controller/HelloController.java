package com.wjk.girl.controller;

import com.wjk.girl.properties.GirlProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by wjk
 *2018/1/23

*/

@RestController
@RequestMapping(value = "/hello")
public class HelloController {
    @Autowired
    private GirlProperties girlProperties;

    /**
    *@Autor 王帮主
    *@Description 廖师兄
    *@param
    *@Date: 9:46 2018/3/16
    */
    //@RequestMapping(value="/say")
    @GetMapping(value = "/say")//组合注解
    public String say(@RequestParam(value = "id",required = false,defaultValue = "0") Integer mId){
        //return girlProperties.getCupSize();
        return "id:"+mId;
    }
    /**
    *@Autor 王帮主
    *@Description csdn
    *@param
    *@Date: 9:43 2018/3/16
    */
    @RequestMapping
    public String hello(){
        return "hello spring-boot";
    }
    /**
    *@Autor 王帮主
    *@Description csdn
    *@param
    *@Date: 9:46 2018/3/16
    */
    @RequestMapping(value = "/info")
    public Map<String,String> getInfo(@RequestParam String name){
        Map<String,String> map = new HashMap<>();
        map.put("name",name);
        return map;
    }

    @RequestMapping(value = "/list")
    public List<Map<String,String>> getList(){
        List<Map<String, String>> list = new ArrayList<>();
        Map<String, String> map = null;
        for (int i = 1; i <= 5; i++) {
            map = new HashMap<>();
            map.put("name", "WangJiankun-" + i);
            list.add(map);
        }
        return list;
    }
    @RequestMapping(value = "/getWechat",method = RequestMethod.POST)
    public Object getWechetInfo(){

        return null;
    }
}
