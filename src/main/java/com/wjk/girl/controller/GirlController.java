package com.wjk.girl.controller;

import com.mascloud.sdkclient.Client;
import com.wjk.girl.domain.Girl;
import com.wjk.girl.domain.Result;
import com.wjk.girl.repository.GirlRepository;
import com.wjk.girl.service.GirlService;
import com.wjk.girl.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class GirlController {
    @Autowired
    private GirlRepository girlRepository;
    @Autowired
    private GirlService girlService;
    private final static Logger logger = LoggerFactory.getLogger(GirlController.class);
    /**
    *@Autor 王帮主
    *@Description 获取所有女生列表
    *@Date: 21:37 2018/1/27
    */
    @GetMapping(value = "/girls")
    public List<Girl> girlList(){
        Client client = Client.getInstance();
        return girlRepository.findAll();
    }
/**
*@Autor 王帮主
*@Description 添加一个女生
*@Date: 21:45 2018/1/27
*/
    @PostMapping(value = "/girls")
    public Result<Girl> girlAdd(@Valid Girl girl, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return null;
            //return ResultUtil.error(1,bindingResult.getFieldError().getDefaultMessage());
        }
        girl.setAge(girl.getAge());
        girl.setCupSize(girl.getCupSize());

        return ResultUtil.success(girlRepository.save(girl));
    }
/**
*@Autor 王帮主
*@Description 通过id获取一个女生的信息
*@Date: 22:08 2018/1/27
*/
    @GetMapping(value = "/girls/{id}")
    public Girl girlFindOne(@PathVariable("id") Integer id){
        return girlRepository.findOne(id);
    }
    /**
    *@Autor 王帮主
    *@Description 更新一个女生信息
    *@param
    *@Date: 20:46 2018/1/28
    */
    @PutMapping(value = "/girls/{id}")
    public Girl updategirl(@PathVariable("id") Integer id,
                               @RequestParam("cupSize") String cupSize,
                           @RequestParam("age") Integer age ){
        Girl girl = new Girl();
        girl.setId(id);
        girl.setCupSize(cupSize);
        girl.setAge(age);
        return girlRepository.save(girl);
    }

    @DeleteMapping(value = "/girls/{id}")
    public void girlDelete(@PathVariable("id") Integer id){
        girlRepository.delete(id);
    }
    @GetMapping(value = "/girls/age/{age}")
    public List<Girl> girlListByAge(@PathVariable("age") Integer age){
        return girlRepository.findByAge(age);
    }
    @PostMapping(value = "/girls/two")
    public void insertTwo(){
        girlService.insertTwo();
    }
    @GetMapping(value = "/girls/getAge/{id}")
    public void getAge(@PathVariable("id") Integer id) throws Exception{
        girlService.getAge(id);
    }
    @PostMapping(value = "/girls/postTest")
    public String postTest(Object obj){
        /*Girl girl = new Girl();
        if(girl != null){
            int id = girl.getId();
            String cupSize = girl.getCupSize();
        }else{
            logger.info("对象获取失败!");
        }*/

        return "hello";
    }
}
