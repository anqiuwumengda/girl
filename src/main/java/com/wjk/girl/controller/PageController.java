package com.wjk.girl.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.Map;

/**
*@Autor 王帮主
*@Description scdn
*@param
*@Date: 10:23 2018/3/16
*/
@Controller
@RequestMapping("/page")
public class PageController {
    @Value("${application.hello}")
    private String hello="hello wjk";

    @RequestMapping(value = "/test")
    public String index(Map<String,Object> model){
        // 直接返回字符串，框架默认会去 spring.view.prefix 目录下的 （index拼接spring.view.suffix）页面
        // 本例为 /WEB-INF/jsp/index.jsp
        model.put("time", new Date());
        model.put("message", this.hello);
        return "test";
    }
    /**
    *@Autor 王帮主
    *@Description  响应到JSP页面page1
    *@param
    *@Date: 10:39 2018/3/16
    */
    @RequestMapping("/page1")
    public ModelAndView page1(){
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        ModelAndView model = new ModelAndView("page/page1");
        model.addObject("content",hello);
        return model;
    }
    /**
    *@Autor 王帮主
    *@Description 响应到JSP页面page1（可以直接使用Model封装内容，直接返回页面字符串）
    *@param
    *@Date: 10:57 2018/3/16
    */
    @RequestMapping("/page2")
    public String page2(Model model){
        // 页面位置 /WEB-INF/views/page/page.jsp
        model.addAttribute("content",hello + "（第二种）");
        return "page/page1";
    }
    @RequestMapping(value = "/testJs")
    public String testJs(){
        return "redirect:/html/gallery.html";
    }
}
