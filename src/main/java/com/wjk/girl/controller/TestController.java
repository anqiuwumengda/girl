package com.wjk.girl.controller;

import com.wjk.girl.domain.wechat.AccessToken;
import com.wjk.girl.utils.wechat.WeixinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "/test")
public class TestController {
    private Logger logger = LoggerFactory.getLogger(TestController.class);
    @GetMapping(value = "/accessToken/{id}")
    public String getAccessToken(@PathVariable("id") String id){
        AccessToken accessToken = WeixinUtil.getAccessToken();
        String result = "id:"+id+",\n accessToken:"+accessToken.getAccessToken()+",\n expiresIn:"+accessToken.getExpiresIn() + "\r\n";
        logger.info(result);
        String filePath = "D:/imgs/1.jpg";
        try {
            String mediaId = WeixinUtil.upload(filePath,accessToken.getAccessToken(),"image");
            logger.info("mediaId:"+mediaId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
