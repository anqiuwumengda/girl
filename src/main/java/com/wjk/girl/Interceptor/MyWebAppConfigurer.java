package com.wjk.girl.Interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
*@Autor 王帮主
*@Description 配置使拦截器生效/资源映射
*@param 
*@Date: 17:13 2018/4/4
*/
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new MyIntercepter1()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    /**
    *@Autor 王帮主
    *@Description
     *  默认配置的 /** 映射到 /static （或/public、/resources、/META-INF/resources）
     *  但是META-INF/resources > resources > static > public
     * 如要访问 static/img/2.png :http://localhost:8090/girl/img/2.png
    *@param 
    *@Date: 17:31 2018/4/4
    */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //1.自定义了一个文件夹，通过设置可以当做默认的路径
        registry.addResourceHandler("/myres/**").addResourceLocations("classpath:/myres/");
        // 2.可以直接使用addResourceLocations 指定磁盘绝对路径，同样可以配置多个位置，注意路径写法需要加上file:
        registry.addResourceHandler("/myimgs/**").addResourceLocations("file:D:/imgs/");
        super.addResourceHandlers(registry);
    }
}
